class ContentController < ApplicationController
  def homepage
    respond_to do |format|
      format.html
      format.json
    end
  end
end
