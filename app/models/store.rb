class Store < ActiveRecord::Base
  attr_accessible :name, :url

  belongs_to :category
end
