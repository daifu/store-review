# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# cat1 = Category.create(name: 'Clothing')
# Category.create(name: 'Electronics')
# Category.create(name: 'Computers')
# Category.create(name: 'Home & Garden')
# Category.create(name: 'Appliances')

# store = Store.create(name: 'Test Store', url: 'http://www.google.com')
# store.category = Category.first
# store.save

