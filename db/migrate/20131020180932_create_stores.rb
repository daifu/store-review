class CreateStores < ActiveRecord::Migration
  def up
    create_table :stores do |t|
      t.string :name
      t.string :url
      t.integer :category_id
      t.timestamps
    end
  end

  def down
    drop_table :stores
  end
end
