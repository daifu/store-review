class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :sub_category_id

      t.timestamps
    end

    add_index(:categories, :name)
    add_index(:categories, :sub_category_id)
  end
end
