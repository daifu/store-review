class CreateSurveyQuestionScores < ActiveRecord::Migration
  def up
    create_table :survey_question_scores do |t|
      t.integer :review_id
      t.integer :survey_question_id
      t.timestamps
    end
  end

  def down
    drop_table :survey_question_scores
  end
end
