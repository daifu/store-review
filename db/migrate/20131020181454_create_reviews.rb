class CreateReviews < ActiveRecord::Migration
  def up
    create_table :reviews do |t|
      t.text :comment, :limit => 5000
      t.boolean :solicited_by_store
      t.string :invoice_number
      t.datetime :transaction_date
      t.integer :survey_id
      t.timestamps
    end
  end

  def down
    drop_table :reviews
  end
end
