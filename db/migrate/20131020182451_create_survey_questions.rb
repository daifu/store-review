class CreateSurveyQuestions < ActiveRecord::Migration
  def up
    create_table :survey_questions do |t|
      t.string :question

      t.timestamps
    end

    # Add default questions
    connection.execute("INSERT INTO survey_questions (question, created_at, updated_at) VALUES ('Rate your overall satisfaction with {{store_name}}.', NOW(), NOW())")
    connection.execute("INSERT INTO survey_questions (question, created_at, updated_at) VALUES ('Rate the cost of {{store_name}}&#39;s products and services.', NOW(), NOW())")
    connection.execute("INSERT INTO survey_questions (question, created_at, updated_at) VALUES ('How likely are you to shop at {{store_name}} in the future?', NOW(), NOW())")
    connection.execute("INSERT INTO survey_questions (question, created_at, updated_at) VALUES ('Rate {{store_name}}&#39;s product Shipping Process overall (speed, no breakage, shipping cost, etc).', NOW(), NOW())")
    connection.execute("INSERT INTO survey_questions (question, created_at, updated_at) VALUES ('Rate {{store_name}}&#39;s Customer Service overall, or choose n/a if you did not interact with their customer service.', NOW(), NOW())")
    connection.execute("INSERT INTO survey_questions (question, created_at, updated_at) VALUES ('If you returned a product for refund or exchange, rate {{store_name}}&#39;s returns service overall, or choose n/a.', NOW(), NOW())")
  end

  def down
    drop_table :survey_questions
  end
end
